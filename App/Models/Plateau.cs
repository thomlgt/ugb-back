﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Plateau
    {
        public Plateau()
        {
            Personnage = new HashSet<Personnage>();
        }

        public int Id { get; set; }
        public int PartieId { get; set; }

        public virtual Partie Partie { get; set; }
        public virtual ICollection<Personnage> Personnage { get; set; }
    }
}
