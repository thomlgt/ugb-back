﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Message
    {
        public long Id { get; set; }
        public string Texte { get; set; }
        public DateTime Date { get; set; }
        public int PersonnageId { get; set; }

        public virtual Personnage Personnage { get; set; }
    }
}
