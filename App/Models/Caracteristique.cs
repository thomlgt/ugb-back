﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Caracteristique
    {
        public int Id { get; set; }
        public int PersonnageId { get; set; }

        public virtual Personnage Personnage { get; set; }
    }
}
