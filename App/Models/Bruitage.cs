﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Bruitage
    {
        public int IdBruitage { get; set; }
        public string Nom { get; set; }
        public string Lien { get; set; }
    }
}
