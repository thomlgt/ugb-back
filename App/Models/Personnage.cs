﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Personnage
    {
        public Personnage()
        {
            Caracteristique = new HashSet<Caracteristique>();
            JetDes = new HashSet<JetDes>();
            Message = new HashSet<Message>();
        }

        public int Id { get; set; }
        public int? PlateauId { get; set; }
        public int AppartenirId { get; set; }
        public string Nom { get; set; }
        public int? X { get; set; }
        public int? Y { get; set; }
        public int Longueur { get; set; }
        public int Largeur { get; set; }

        public virtual Appartenir Appartenir { get; set; }
        public virtual Plateau Plateau { get; set; }
        public virtual ICollection<Caracteristique> Caracteristique { get; set; }
        public virtual ICollection<JetDes> JetDes { get; set; }
        public virtual ICollection<Message> Message { get; set; }
    }
}
