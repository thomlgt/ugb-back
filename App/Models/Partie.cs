﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Partie
    {
        public Partie()
        {
            Appartenir = new HashSet<Appartenir>();
            Plateau = new HashSet<Plateau>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public DateTime DateCreation { get; set; }

        public virtual ICollection<Appartenir> Appartenir { get; set; }
        public virtual ICollection<Plateau> Plateau { get; set; }
    }
}
