﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Appartenir
    {
        public Appartenir()
        {
            Personnage = new HashSet<Personnage>();
        }

        public int Id { get; set; }
        public sbyte MaitreJeu { get; set; }
        public int PartieId { get; set; }
        public int JoueurId { get; set; }

        public virtual Joueur Joueur { get; set; }
        public virtual Partie Partie { get; set; }
        public virtual ICollection<Personnage> Personnage { get; set; }
    }
}
