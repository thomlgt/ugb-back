﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Utilisateur
    {
        public Utilisateur()
        {
            Joueur = new HashSet<Joueur>();
        }

        public int Id { get; set; }
        public string MotDePasse { get; set; }
        public string Mail { get; set; }

        public virtual ICollection<Joueur> Joueur { get; set; }
    }
}
