﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Log
    {
        public long Id { get; set; }
        public string Texte { get; set; }
        public DateTime Date { get; set; }
    }
}
