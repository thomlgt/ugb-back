﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class JetDes
    {
        public long Id { get; set; }
        public string Resultat { get; set; }
        public string Calcul { get; set; }
        public DateTime Date { get; set; }
        public int PersonnageId { get; set; }

        public virtual Personnage Personnage { get; set; }
    }
}
