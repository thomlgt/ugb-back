﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public partial class Joueur
    {
        public Joueur()
        {
            Appartenir = new HashSet<Appartenir>();
        }

        public int Id { get; set; }
        public string Pseudo { get; set; }
        public int UtilisateurId { get; set; }

        public virtual Utilisateur Utilisateur { get; set; }
        public virtual ICollection<Appartenir> Appartenir { get; set; }
    }
}
