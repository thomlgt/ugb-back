﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IAppartenirRepository : IDisposable
    {
        IEnumerable FindAll();
        Appartenir FindById(int id);
        void Insert(Appartenir contenu);
        void Delete(int id);
        void Update(Appartenir contenu);
        void Save();
    }
}
