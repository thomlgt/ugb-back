﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IPlateauRepository : IDisposable
    {
        IEnumerable FindAll();
        Plateau FindById(int id);
        void Insert(Plateau plateau);
        void Delete(int id);
        void Update(Plateau plateau);
        void Save();
    }
}
