﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IJoueurRepository : IDisposable
    {
        IEnumerable FindAll();
        Joueur FindById(int id);
        void Insert(Joueur joueur);
        void Delete(int id);
        void Update(Joueur joueur);
        void Save();
    }
}
