﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IPartieRepository : IDisposable
    {
        IEnumerable FindAll();
        Partie FindById(int id);
        void Insert(Partie partie);
        void Delete(int id);
        void Update(Partie partie);
        void Save();
    }
}
