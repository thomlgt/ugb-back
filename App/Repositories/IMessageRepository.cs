﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IMessageRepository : IDisposable
    {
        IEnumerable FindAll();
        Message FindById(int id);
        void Insert(Message message);
        void Delete(int id);
        void Update(Message message);
        void Save();
    }
}
