﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class MessageRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public MessageRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER MESSAGE
        public void Delete(int id)
        {
            Message message = context.Message.Find(id);
            context.Message.Remove(message);
        }

        //TROUVER TOUS LES MESSAGES
        public IEnumerable FindAll()
        {
            return context.Message;
        }

        //TROUVER UN MESSAGE AVEC SON {id}
        public Message FindById(int id)
        {
            return context.Message.Find(id);
        }

        //AJOUTER UN MESSAGE
        public void Insert(Message message)
        {
            context.Message.Add(message);
        }

        //MODIFIER UN MESSAGE
        public void Update(Message message)
        {
            context.Entry(message).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
