﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class UtilisateurRepository : IUtilisateurRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public UtilisateurRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER UTILISATEUR
        public void Delete(int id)
        {
            Utilisateur utilisateur = context.Utilisateur.Find(id);
            context.Utilisateur.Remove(utilisateur);
        }

        //TROUVER TOUS LES UTILISATEURS
        public IEnumerable FindAll()
        {
            return context.Utilisateur;
        }

        //TROUVER UN UTILISATEUR AVEC SON {id}
        public Utilisateur FindById(int id)
        {
            return context.Utilisateur.Find(id);
        }

        //AJOUTER UN UTILISATEUR
        public void Insert(Utilisateur utilisateur)
        {
            context.Utilisateur.Add(utilisateur);
        }

        //MODIFIER UN UTILISATEUR
        public void Update(Utilisateur utilisateur)
        {
            context.Entry(utilisateur).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
