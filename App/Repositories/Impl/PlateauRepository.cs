﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class PlateauRepository : IPlateauRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public PlateauRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER PLATEAU
        public void Delete(int id)
        {
            Plateau plateau = context.Plateau.Find(id);
            context.Plateau.Remove(plateau);
        }

        //TROUVER TOUS LES PLATEAUX
        public IEnumerable FindAll()
        {
            return context.Plateau;
        }

        //TROUVER UN PLATEAU AVEC SON {id}
        public Plateau FindById(int id)
        {
            return context.Plateau.Find(id);
        }

        //AJOUTER UN PLATEAU
        public void Insert(Plateau plateau)
        {
            context.Plateau.Add(plateau);
        }

        //MODIFIER UN PLATEAU
        public void Update(Plateau plateau)
        {
            context.Entry(plateau).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
