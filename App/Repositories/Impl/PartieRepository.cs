﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class PartieRepository : IPartieRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public PartieRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER PARTIE
        public void Delete(int id)
        {
            Partie partie = context.Partie.Find(id);
            context.Partie.Remove(partie);
        }

        //TROUVER TOUS LES PARTIES
        public IEnumerable FindAll()
        {
            return context.Partie;
        }

        //TROUVER UNE PARTIE AVEC SON {id}
        public Partie FindById(int id)
        {
            return context.Partie.Find(id);
        }

        //AJOUTER UNE PARTIE
        public void Insert(Partie partie)
        {
            context.Partie.Add(partie);
        }

        //MODIFIER UNE PARTIE
        public void Update(Partie partie)
        {
            context.Entry(partie).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
