﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class AppartenirRepository : IAppartenirRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public AppartenirRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER APPARTENIR
        public void Delete(int id)
        {
            Appartenir contenu = context.Appartenir.Find(id);
            context.Appartenir.Remove(contenu);
        }

        //TROUVER TOUS LES APPARTENIRS
        public IEnumerable FindAll()
        {
            return context.Appartenir;
        }

        //TROUVER UN APPARTENIR AVEC SON {id}
        public Appartenir FindById(int id)
        {
            return context.Appartenir.Find(id);
        }

        //AJOUTER UN APPARTENIR
        public void Insert(Appartenir contenu)
        {
            context.Appartenir.Add(contenu);
        }

        //MODIFIER UN APPARTENIR
        public void Update(Appartenir contenu)
        {
            context.Entry(contenu).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
