﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class JetDesRepository : IJetDesRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public JetDesRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER JET DE DES
        public void Delete(int id)
        {
            JetDes jet = context.JetDes.Find(id);
            context.JetDes.Remove(jet);
        }

        //TROUVER TOUS LES JET DE DES
        public IEnumerable FindAll()
        {
            return context.JetDes;
        }

        //TROUVER UN JET DE DES AVEC SON {id}
        public JetDes FindById(int id)
        {
            return context.JetDes.Find(id);
        }

        //AJOUTER UN JET DE DES
        public void Insert(JetDes jet)
        {
            context.JetDes.Add(jet);
        }

        //MODIFIER UN JET DE DES
        public void Update(JetDes jet)
        {
            context.Entry(jet).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
