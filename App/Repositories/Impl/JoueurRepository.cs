﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class JoueurRepository : IJoueurRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public JoueurRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER JOUEUR
        public void Delete(int id)
        {
            Joueur joueur = context.Joueur.Find(id);
            context.Joueur.Remove(joueur);
        }

        //TROUVER TOUS LES JOUEURS
        public IEnumerable FindAll()
        {
            return context.Joueur;
        }

        //TROUVER UN JOUEUR AVEC SON {id}
        public Joueur FindById(int id)
        {
            return context.Joueur.Find(id);
        }

        //AJOUTER UN JOUEUR
        public void Insert(Joueur joueur)
        {
            context.Joueur.Add(joueur);
        }

        //MODIFIER UN JOUEUR
        public void Update(Joueur joueur)
        {
            context.Entry(joueur).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
