﻿using App.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories.Impl
{
    public class PersonnageRepository : IPersonnageRepository
    {
        //INITIALISATION DU CONTEXT
        private UgbContext context;

        //CONSTRUCTEUR
        public PersonnageRepository(UgbContext context)
        {
            this.context = context;
        }

        //SUPPRIMER PERSONNAGE
        public void Delete(int id)
        {
            Personnage personnage = context.Personnage.Find(id);
            context.Personnage.Remove(personnage);
        }

        //TROUVER TOUS LES PERSONNAGEs
        public IEnumerable FindAll()
        {
            return context.Personnage;
        }

        //TROUVER UN PERSONNAGE AVEC SON {id}
        public Personnage FindById(int id)
        {
            return context.Personnage.Find(id);
        }

        //AJOUTER UN PERSONNAGE
        public void Insert(Personnage personnage)
        {
            context.Personnage.Add(personnage);
        }

        //MODIFIER UN PERSONNAGE
        public void Update(Personnage personnage)
        {
            context.Entry(personnage).State = EntityState.Modified;
        }

        //SAUVEGARDER LES CHANGEMENTS
        public void Save()
        {
            context.SaveChanges();
        }

        //LIBÉRATION DES RESSOURCES UTILISÉES PAR LE CONTEXT
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
