﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IUtilisateurRepository : IDisposable
    {
        IEnumerable FindAll();
        Utilisateur FindById(int id);
        void Insert(Utilisateur utilisateur);
        void Delete(int id);
        void Update(Utilisateur utilisateur);
        void Save();
    }
}
