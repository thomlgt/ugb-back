﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IJetDesRepository : IDisposable
    {
        IEnumerable FindAll();
        JetDes FindById(int id);
        void Insert(JetDes jet);
        void Delete(int id);
        void Update(JetDes jet);
        void Save();
    }
}
