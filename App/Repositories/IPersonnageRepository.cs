﻿using App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IPersonnageRepository : IDisposable
    {
        IEnumerable FindAll();
        Personnage FindById(int id);
        void Insert(Personnage personnage);
        void Delete(int id);
        void Update(Personnage personnage);
        void Save();
    }
}
