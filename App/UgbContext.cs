﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace App.Models
{
    public partial class UgbContext : DbContext
    {
        public UgbContext()
        {
        }

        public UgbContext(DbContextOptions<UgbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Appartenir> Appartenir { get; set; }
        public virtual DbSet<Bruitage> Bruitage { get; set; }
        public virtual DbSet<Caracteristique> Caracteristique { get; set; }
        public virtual DbSet<JetDes> JetDes { get; set; }
        public virtual DbSet<Joueur> Joueur { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Partie> Partie { get; set; }
        public virtual DbSet<Personnage> Personnage { get; set; }
        public virtual DbSet<Plateau> Plateau { get; set; }
        public virtual DbSet<Utilisateur> Utilisateur { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;password=root;database=ugb", x => x.ServerVersion("8.0.19-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appartenir>(entity =>
            {
                entity.ToTable("appartenir");

                entity.HasIndex(e => e.JoueurId)
                    .HasName("fk_appartenir_joueur1_idx");

                entity.HasIndex(e => e.PartieId)
                    .HasName("fk_appartenir_salon1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.JoueurId).HasColumnName("joueur_id");

                entity.Property(e => e.MaitreJeu).HasColumnName("maitre_jeu");

                entity.Property(e => e.PartieId).HasColumnName("partie_id");

                entity.HasOne(d => d.Joueur)
                    .WithMany(p => p.Appartenir)
                    .HasForeignKey(d => d.JoueurId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_appartenir_joueur1");

                entity.HasOne(d => d.Partie)
                    .WithMany(p => p.Appartenir)
                    .HasForeignKey(d => d.PartieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_appartenir_salon1");
            });

            modelBuilder.Entity<Bruitage>(entity =>
            {
                entity.HasKey(e => e.IdBruitage)
                    .HasName("PRIMARY");

                entity.ToTable("bruitage");

                entity.Property(e => e.IdBruitage).HasColumnName("id_bruitage");

                entity.Property(e => e.Lien)
                    .HasColumnName("lien")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Nom)
                    .HasColumnName("nom")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Caracteristique>(entity =>
            {
                entity.ToTable("caracteristique");

                entity.HasIndex(e => e.PersonnageId)
                    .HasName("fk_caracteristique_personnage1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PersonnageId).HasColumnName("personnage_id");

                entity.HasOne(d => d.Personnage)
                    .WithMany(p => p.Caracteristique)
                    .HasForeignKey(d => d.PersonnageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_caracteristique_personnage1");
            });

            modelBuilder.Entity<JetDes>(entity =>
            {
                entity.ToTable("jet_des");

                entity.HasIndex(e => e.PersonnageId)
                    .HasName("fk_jet_des_personnage1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Calcul)
                    .IsRequired()
                    .HasColumnName("calcul")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PersonnageId).HasColumnName("personnage_id");

                entity.Property(e => e.Resultat)
                    .IsRequired()
                    .HasColumnName("resultat")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.Personnage)
                    .WithMany(p => p.JetDes)
                    .HasForeignKey(d => d.PersonnageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_jet_des_personnage1");
            });

            modelBuilder.Entity<Joueur>(entity =>
            {
                entity.ToTable("joueur");

                entity.HasIndex(e => e.Id)
                    .HasName("fk_joueur_utilisateur_idx");

                entity.HasIndex(e => e.UtilisateurId)
                    .HasName("fk_joueur_utilisateur1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Pseudo)
                    .IsRequired()
                    .HasColumnName("pseudo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UtilisateurId).HasColumnName("utilisateur_id");

                entity.HasOne(d => d.Utilisateur)
                    .WithMany(p => p.Joueur)
                    .HasForeignKey(d => d.UtilisateurId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_joueur_utilisateur1");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.ToTable("log");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Texte)
                    .IsRequired()
                    .HasColumnName("texte")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.ToTable("message");

                entity.HasIndex(e => e.PersonnageId)
                    .HasName("fk_message_personnage1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PersonnageId).HasColumnName("personnage_id");

                entity.Property(e => e.Texte)
                    .IsRequired()
                    .HasColumnName("texte")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.Personnage)
                    .WithMany(p => p.Message)
                    .HasForeignKey(d => d.PersonnageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_message_personnage1");
            });

            modelBuilder.Entity<Partie>(entity =>
            {
                entity.ToTable("partie");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateCreation)
                    .HasColumnName("date_creation")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasColumnType("varchar(60)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Personnage>(entity =>
            {
                entity.ToTable("personnage");

                entity.HasIndex(e => e.AppartenirId)
                    .HasName("fk_personnage_appartenir1_idx");

                entity.HasIndex(e => e.PlateauId)
                    .HasName("fk_pion_plateau1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AppartenirId).HasColumnName("appartenir_id");

                entity.Property(e => e.Largeur).HasColumnName("largeur");

                entity.Property(e => e.Longueur).HasColumnName("longueur");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.PlateauId).HasColumnName("plateau_id");

                entity.Property(e => e.X).HasColumnName("x");

                entity.Property(e => e.Y).HasColumnName("y");

                entity.HasOne(d => d.Appartenir)
                    .WithMany(p => p.Personnage)
                    .HasForeignKey(d => d.AppartenirId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_personnage_appartenir1");

                entity.HasOne(d => d.Plateau)
                    .WithMany(p => p.Personnage)
                    .HasForeignKey(d => d.PlateauId)
                    .HasConstraintName("fk_pion_plateau1");
            });

            modelBuilder.Entity<Plateau>(entity =>
            {
                entity.ToTable("plateau");

                entity.HasIndex(e => e.PartieId)
                    .HasName("fk_plateau_partie1_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PartieId).HasColumnName("partie_id");

                entity.HasOne(d => d.Partie)
                    .WithMany(p => p.Plateau)
                    .HasForeignKey(d => d.PartieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_plateau_partie1");
            });

            modelBuilder.Entity<Utilisateur>(entity =>
            {
                entity.ToTable("utilisateur");

                entity.HasIndex(e => e.Mail)
                    .HasName("mail")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Mail)
                    .IsRequired()
                    .HasColumnName("mail")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.MotDePasse)
                    .IsRequired()
                    .HasColumnName("mot_de_passe")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
